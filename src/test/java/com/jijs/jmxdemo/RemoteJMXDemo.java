package com.jijs.jmxdemo;

import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import com.jijs.jmx.standard.HelloMBean;

public class RemoteJMXDemo {

	public void jvmClassLoading(String jmxURL) throws Exception {

		JMXServiceURL address = new JMXServiceURL(jmxURL);
		JMXConnector connector = JMXConnectorFactory.connect(address);
		MBeanServerConnection mbs = connector.getMBeanServerConnection();

		try {
			ClassLoadingMXBean classLoadingMXBean = ManagementFactory.newPlatformMXBeanProxy(mbs,
					ManagementFactory.CLASS_LOADING_MXBEAN_NAME, ClassLoadingMXBean.class);

			System.out.println(classLoadingMXBean.getLoadedClassCount());
			System.out.println(classLoadingMXBean.getTotalLoadedClassCount());
			System.out.println(classLoadingMXBean.getUnloadedClassCount());
			System.out.println(classLoadingMXBean.isVerbose());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void helloWorld(String jmxURL) throws Exception{
		JMXServiceURL address = new JMXServiceURL(jmxURL);
		JMXConnector connector = JMXConnectorFactory.connect(address);
		MBeanServerConnection mbs = connector.getMBeanServerConnection();

		try {
			HelloMBean helloMbean = ManagementFactory.newPlatformMXBeanProxy(mbs,
					ManagementFactory.CLASS_LOADING_MXBEAN_NAME, HelloMBean.class);

			System.out.println(helloMbean.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception{
		String jmxurl = "service:jmx:rmi:///jndi/rmi://192.168.31.186:9999/jmxrmi";
		RemoteJMXDemo t = new RemoteJMXDemo();
//		t.jvmClassLoading(jmxurl);
		t.helloWorld(jmxurl);
	}
}
