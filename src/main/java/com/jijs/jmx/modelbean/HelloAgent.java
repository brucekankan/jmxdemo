package com.jijs.jmx.modelbean;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.modelmbean.RequiredModelMBean;

import com.sun.jdmk.comm.HtmlAdaptorServer;

public class HelloAgent {

	/**
	 * 运行HelloAgent，然后打开网页： http://localhost:8082/,看效果!
	 * 
	 * 我们发现模型 Mbean(Model MBean)要比标准 MBean(standard mbean)复杂 多了，那有什么理由让我们选择使用模型
	 * MBean 吗？我认为，最大的理由就是模 型 MBean 可以动态配置。试想一下这个应用场景：由于安 全或其他原因，系统 要把某个 MBean
	 * 公开的可管理方法隐藏起来。这时，如果你是用标准 MBean，这 需要修改接口类，然后重新编译发布；如果用 Apache
	 * commons-modeler 来写的 模型 MBean，则只需要修改 XML 文件就行了，不需要重新编译发布（可能要重启 一下系统）。这就是模型
	 * Mbean 优势之所在了。
	 * 
	 * @param args
	 * @throws MalformedObjectNameException
	 * @throws InstanceAlreadyExistsException
	 * @throws MBeanRegistrationException
	 * @throws NotCompliantMBeanException
	 */
	public static void main(String[] args) throws MalformedObjectNameException, InstanceAlreadyExistsException,
			MBeanRegistrationException, NotCompliantMBeanException {

		MBeanServer server = MBeanServerFactory.createMBeanServer();
		ObjectName helloName = new ObjectName("jijs:name=HelloWorld");

		// Hello hello = new Hello();
		RequiredModelMBean hello = ModelMBeanUtils.createModlerMBean();
		server.registerMBean(hello, helloName);

		ObjectName adapterName = new ObjectName("HelloAgent:name=htmladapter,port=8082");
		HtmlAdaptorServer adapter = new HtmlAdaptorServer();
		server.registerMBean(adapter, adapterName);
		adapter.start();
		System.out.println("start.....");
	}
}
