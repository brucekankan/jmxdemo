package com.jijs.jmx.modeler;

import java.io.InputStream;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.modelmbean.ModelMBean;
import org.apache.commons.modeler.ManagedBean;
import org.apache.commons.modeler.Registry;
import com.sun.jdmk.comm.HtmlAdaptorServer;

public class HelloAgent {
	public static void main(String[] args) throws Exception {
		// 基于 xml 中的信息构建一个 Registry
		Registry registry = Registry.getRegistry(null, null);
		InputStream stream = HelloAgent.class.getResourceAsStream("Mbeans-descriptors.xml");
		registry.loadMetadata(stream);
		stream.close();
		// 由 Registry 得到一个 MBeanServer
		MBeanServer server = registry.getMBeanServer();

		// 得到 Hello 在描述文件中的信息类，对应于 xml 文件<mbean>标签的 name 属性。
		ManagedBean managed = registry.findManagedBean("Hello");
		// 创建 ObjectName
		ObjectName helloName = new ObjectName(managed.getDomain() + ":name=HelloWorld");
		// 得到 ModelMBean
		ModelMBean hello = managed.createMBean(new Hello());
		// 注册 MBean
		server.registerMBean(hello, helloName);

		ObjectName adapterName = new ObjectName("HelloAgent:name=htmladapter,port=8082");
		HtmlAdaptorServer adapter = new HtmlAdaptorServer();
		server.registerMBean(adapter, adapterName);
		adapter.start();
		System.out.println("start.....");
	}
}

/**
 * 
 */
