package com.jijs.jmx.modeler.demo;

public class TestBean {
	private String oneAttr;

	public String getOneAttr() {
		return "one attribute be testing";
	}

	public void setOneAttr(String attr) {
		this.oneAttr = attr;
	}

	public String toString() {
		return "toString be testing";
	}
}
