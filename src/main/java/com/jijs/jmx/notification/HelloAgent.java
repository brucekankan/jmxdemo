package com.jijs.jmx.notification;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import com.sun.jdmk.comm.HtmlAdaptorServer;

public class HelloAgent {

	/**
	 * 1、先运行HelloAgent启动服务，再打开浏览器输入网 址： http://localhost:8082/
	 * 
	 * 2、 进入“name=jack”项，然后单击“hi”按钮来执行它
	 * 
	 * @param args
	 * @throws MalformedObjectNameException
	 * @throws InstanceAlreadyExistsException
	 * @throws MBeanRegistrationException
	 * @throws NotCompliantMBeanException
	 */
	public static void main(String[] args) throws MalformedObjectNameException,
			InstanceAlreadyExistsException, MBeanRegistrationException,
			NotCompliantMBeanException {

		MBeanServer server = MBeanServerFactory.createMBeanServer();
		
		ObjectName helloName = new ObjectName("jijs:name=HelloWorld");
		Hello hello = new Hello();
		server.registerMBean(hello, helloName);

		ObjectName adapterName = new ObjectName(
				"HelloAgent:name=htmladapter,port=8082");
		HtmlAdaptorServer adapter = new HtmlAdaptorServer();
		server.registerMBean(adapter, adapterName);

		//重点
		ObjectName jackName = new ObjectName("HelloAgent:name=jack");
		Jack jack = new Jack();
		server.registerMBean(jack, jackName);
		HelloListener listener = new HelloListener();
		jack.addNotificationListener(listener, null, hello);
		jack.addNotificationListener(listener, null, jack);

		adapter.start();
		System.out.println("start.....");
	}
}
