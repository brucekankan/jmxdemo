package com.jijs.jmx.notification;

import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

public class Jack extends NotificationBroadcasterSupport implements JackMBean {

	private int seq = 0;

	/**
	 * 说明：
	 * 
	 * • 必需继承 NotificationBroadcasterSupport
	 * 
	 * • 此类只有一个 hi 方法，方法只有两句：创建一个 Notification 消息包， 然后将包发出去
	 * 
	 * • 如果你还要在消息包上附加其他数据， Notification 还有一个 setUserData 方法可供使用
	 */
	@Override
	public void hi() {
		/**
		 * 创建一个信息包 1、type:给这个Notifaction起一个名称 2、source:由谁发出的Notification
		 * 3、sequenceNumber:一系列通知中的序列号，可以设置任意值 4、timeStamp:发出时间
		 * 5、message:发出的消息文本
		 */
		Notification n = new Notification("jack.hi", this, ++seq,
				System.currentTimeMillis(), "jack");
		// n.setUserData("其它消息");

		// 发出去
		sendNotification(n);
	}

}
