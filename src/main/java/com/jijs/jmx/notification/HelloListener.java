package com.jijs.jmx.notification;

import javax.management.Notification;
import javax.management.NotificationListener;


/**
 * 创建一个 Listener，监听到的 Notification 消息包将由此类负责处理。
 * @author jjs
 *
 */
public class HelloListener implements NotificationListener {

	@Override
	public void handleNotification(Notification n, Object handback) {
		System.out.println("type=" + n.getType());
		System.out.println("source=" + n.getSource());
		System.out.println("seq=" + n.getSequenceNumber());
		System.out.println("send time=" + n.getTimeStamp());
		System.out.println("message=" + n.getMessage());

		if (handback != null) {
			if(handback instanceof Hello){
				Hello hello = (Hello)handback;
				hello.printHello(n.getMessage());
			}else if(handback instanceof Jack){
				System.out.println("================");
				System.out.println("我是Jack");
				System.out.println("================");
			}
		}
	}

}
